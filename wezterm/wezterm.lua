function os.capture(cmd, raw)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  f:close()
  if raw then return s end
  s = string.gsub(s, '^%s+', '')
  s = string.gsub(s, '%s+$', '')
  s = string.gsub(s, '[\n\r]+', ' ')
  return s
end

-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

local act = wezterm.action

wezterm.on('update-right-status', function(window, pane)
	local workspace_name = window:active_workspace()
	local name = window:active_key_table()
	local domain = pane:get_domain_name()
	if not name then
		name = 'normal'
	end
	local res = name .. " <> " .. workspace_name .. " <> " .. domain .. " "
	window:set_right_status(res)
end)

local SUPER_SHIFT = os.capture 'uname' == 'Linux' and 'CTRL|SHIFT' or 'SUPER' 
local SUPER = os.capture 'uname' == 'Linux' and 'CTRL' or 'SUPER' 


-- This is where you actually apply your config choices
config = {
	font = wezterm.font_with_fallback{ "Pixel Code", "FiraCode Nerd Font" },
	font_size = 13.0,
	color_scheme = "Gruvbox dark, soft (base16)",
	window_frame = {
		font = wezterm.font({ family = "Pixel Code" }),
		font_size = 12.0,
		active_titlebar_bg = "#333333",
		inactive_titlebar_bg = "#333333",
	},
	enable_scroll_bar = true,
	window_decorations = "INTEGRATED_BUTTONS|RESIZE|MACOS_FORCE_DISABLE_SHADOW",
	integrated_title_button_alignment = "Left",
	native_macos_fullscreen_mode = true,
	default_cursor_style = 'BlinkingBlock',
	cursor_thickness = "0.1pt",
	cursor_blink_rate = 1000,
	animation_fps = 60,
	cursor_blink_ease_in = 'Linear',
	cursor_blink_ease_out = 'Linear',
	switch_to_last_active_tab_when_closing_tab = true,
	disable_default_key_bindings = true
}

config.key_tables = {
	pane = {
		{ key = 'x', action = act.CloseCurrentPane { confirm = false } },
		{ key = 'x', mods = SUPER_SHIFT, action = act.CloseCurrentPane { confirm = false } },
		{
			key = 'd',
			mods = 'SHIFT',
			action = wezterm.action.SplitPane {
				direction = 'Down',
				size = { Percent = 50 },
				top_level = true,
			},
		},
		{
			key = 'd',
			mods = SUPER_SHIFT .. '|ALT',
			action = wezterm.action.SplitPane {
				direction = 'Down',
				size = { Percent = 50 },
				top_level = true,
			},
		},
		{
			key = 'd',
			action = wezterm.action.SplitPane {
				direction = 'Down',
				size = { Percent = 50 },
			},
		},
		{
			key = 'd',
			mods = SUPER_SHIFT,
			action = wezterm.action.SplitPane {
				direction = 'Down',
				size = { Percent = 50 },
			},
		},
		{
			key = 'r',
			mods = 'SHIFT',
			action = wezterm.action.SplitPane {
				direction = 'Right',
				size = { Percent = 50 },
				top_level = true,
			},
		},
		{
			key = 'r',
			mods = SUPER_SHIFT .. '|ALT',
			action = wezterm.action.SplitPane {
				direction = 'Right',
				size = { Percent = 50 },
				top_level = true,
			},
		},
		{
			key = 'r',
			action = wezterm.action.SplitPane {
				direction = 'Right',
				size = { Percent = 50 },
			},
		},
		{
			key = 'r',
			mods = SUPER_SHIFT,
			action = wezterm.action.SplitPane {
				direction = 'Right',
				size = { Percent = 50 },
			},
		},
		{
			key = 'f',
			action = act.PaneSelect {
				mode = 'Activate',
			}
		},
		{
			key = 'f',
			mods = SUPER_SHIFT,
			action = act.PaneSelect {
				mode = 'Activate',
			}
		},
		{
			key = 'f',
			mods = 'SHIFT',
			action = act.PaneSelect {
				mode = 'SwapWithActive',
			}
		},
		{
			key = 'f',
			mods = SUPER_SHIFT .. '|ALT',
			action = act.PaneSelect {
				mode = 'SwapWithActive',
			}
		},
		{ key = ']', action = act.RotatePanes 'Clockwise' },
		{ key = ']', mods = SUPER_SHIFT, action = act.RotatePanes 'Clockwise' },
		{ key = '[', action = act.RotatePanes 'CounterClockwise' },
		{ key = '[', mods = SUPER_SHIFT, action = act.RotatePanes 'CounterClockwise' },
		{ key = 'z', action = act.TogglePaneZoomState },
		{ key = 'z', mods = SUPER_SHIFT, action = act.TogglePaneZoomState },
	},
	workspace = {
		{
			key = 'n',
			action = act.PromptInputLine {
				description = wezterm.format {
					{ Text = 'Enter name for new workspace' },
				},
				action = wezterm.action_callback(function(window, pane, line)
					if line then
						window:perform_action(
							act.SwitchToWorkspace {
								name = line,
							},
							pane
						)
					end
				end),
			},
		},
		{
			key = 'n',
			mods = SUPER_SHIFT,
			action = act.PromptInputLine {
				description = wezterm.format {
					{ Text = 'Enter name for new workspace' },
				},
				action = wezterm.action_callback(function(window, pane, line)
					if line then
						window:perform_action(
							act.SwitchToWorkspace {
								name = line,
							},
							pane
						)
					end
				end),
			},
		},
		{
			key = 'f',
			action = act.ShowLauncherArgs {
				flags = 'FUZZY|WORKSPACES',
			},
		},
		{
			key = 'f',
			mods = SUPER_SHIFT,
			action = act.ShowLauncherArgs {
				flags = 'FUZZY|WORKSPACES',
			},
		},
		{
			key = 'r',
			action = act.PromptInputLine {
				description = wezterm.format {
					{ Text = 'Enter workspace name' },
				},
				action = wezterm.action_callback(function(_, _, line)
					if line then
						wezterm.mux.rename_workspace(
							wezterm.mux.get_active_workspace(),
							line
						)
					end
				end),
			},
		},
		{
			key = 'r',
			mods = SUPER_SHIFT,
			action = act.PromptInputLine {
				description = wezterm.format {
					{ Text = 'Enter workspace name' },
				},
				action = wezterm.action_callback(function(_, _, line)
					if line then
						wezterm.mux.rename_workspace(
							wezterm.mux.get_active_workspace(),
							line
						)
					end
				end),
			},
		},
	},
	tab = {
		{ key = 'n', action = act.SpawnTab 'CurrentPaneDomain' },
		{ key = 'n', mods = SUPER_SHIFT, action = act.SpawnTab 'CurrentPaneDomain' },
		{ key = ']', action = act.ActivateTabRelative(1) },
		{ key = ']', mods = SUPER_SHIFT, action = act.ActivateTabRelative(1) },
		{ key = '[', action = act.ActivateTabRelative(-1) },
		{ key = '[', mods = SUPER_SHIFT, action = act.ActivateTabRelative(-1) },
		{ key = '}', action = act.MoveTabRelative(1) },
		{ key = '}', mods = SUPER_SHIFT, action = act.MoveTabRelative(1) },
		{ key = '{', action = act.MoveTabRelative(-1) },
		{ key = '{', mods = SUPER_SHIFT, action = act.MoveTabRelative(-1) },
		{
			key = 'r',
			action = act.PromptInputLine {
				description = wezterm.format {
					{ Text = 'Enter tab name' },
				},
				action = wezterm.action_callback(function(window, _, line)
					if line then
						window:active_tab():set_title(line)
					end
				end),
			},
		},
		{
			key = 'r',
			mods = SUPER_SHIFT,
			action = act.PromptInputLine {
				description = wezterm.format {
					{ Text = 'Enter tab name' },
				},
				action = wezterm.action_callback(function(window, _, line)
					if line then
						window:active_tab():set_title(line)
					end
				end),
			},
		},
		{ key = 'x', action = act.CloseCurrentTab { confirm = false }, },
		{ key = 'x', mods = SUPER_SHIFT, action = act.CloseCurrentTab { confirm = false }, },
		{ key = 'o', action = act.ActivateLastTab },
		{ key = 'o', mods = SUPER_SHIFT, action = act.ActivateLastTab },
		{ key = 'f', action = act.ShowTabNavigator },
		{ key = 'f', mods = SUPER_SHIFT, action = act.ShowTabNavigator },
	},
	action = {
		{ key = 'p', action = act.ActivateCommandPalette, },
		{ key = 'p', mods = SUPER_SHIFT, action = act.ActivateCommandPalette, },
		{ key = 'v', action = act.QuickSelect },
		{ key = 'v', mods = SUPER_SHIFT, action = act.QuickSelect },
		{
			key = 'k',
			action = act.Multiple {
				act.ClearScrollback 'ScrollbackAndViewport',
				act.SendKey { key = 'L', mods = 'CTRL' },
			},
		},{
			key = 'k',
			mods = SUPER_SHIFT,
			action = act.Multiple {
				act.ClearScrollback 'ScrollbackAndViewport',
				act.SendKey { key = 'L', mods = 'CTRL' },
			},
		},
		{ key = 'c', action = act.ActivateCopyMode },
		{ key = 'c', mods = SUPER_SHIFT, action = act.ActivateCopyMode },
		{ key = 'f', action = act.Search("CurrentSelectionOrEmptyString") },
		{ key = 'f', mods = SUPER_SHIFT, action = act.Search("CurrentSelectionOrEmptyString") },
	},
}

config.keys = {
	{
		key = 'Escape',
		mods = SUPER,
		action = act.ClearKeyTableStack,
	},
	{
		key = 'f',
		mods = SUPER_SHIFT,
		action = act.ActivateKeyTable {
			name = 'pane',
		}
	},
	{
		key = 'w',
		mods = SUPER_SHIFT,
		action = act.ActivateKeyTable {
			name = 'workspace',
		}
	},
	{
		key = 't',
		mods = SUPER_SHIFT,
		action = act.ActivateKeyTable {
			name = 'tab',
		}
	},
	{
		key = 'a',
		mods = SUPER_SHIFT,
		action = act.ActivateKeyTable {
			name = 'action',
		}
	},
	{
		key = '-',
		mods = SUPER,
		action = act.DecreaseFontSize,
	},
	{
		key = '=',
		mods = SUPER,
		action = act.IncreaseFontSize,
	},
	{
		key = '0',
		mods = SUPER,
		action = act.ResetFontSize,
	},
	{
		key = 'v',
		mods = SUPER_SHIFT,
		action = act.PasteFrom 'Clipboard'
	},
	{
		key = 'c',
		mods = SUPER_SHIFT,
		action = act.CopyTo 'Clipboard',
	},
	{
		key = 'LeftArrow',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Left',
	},
	{
		key = 'h',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Left',
	},
	{
		key = 'UpArrow',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Up',
	},
	{
		key = 'k',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Up',
	},
	{
		key = 'RightArrow',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Right',
	},
	{
		key = 'l',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Right',
	},
	{
		key = 'DownArrow',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Down',
	},
	{
		key = 'j',
		mods = SUPER_SHIFT,
		action = act.ActivatePaneDirection 'Down',
	},
	{
		key = 'LeftArrow',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Left', 1 },
	},
	{
		key = 'h',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Left', 1 },
	},
	{
		key = 'UpArrow',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Up', 1 },
	},
	{
		key = 'k',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Up', 1 },
	},
	{
		key = 'RightArrow',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Right', 1 },
	},
	{
		key = 'l',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Right', 1 },
	},
	{
		key = 'DownArrow',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Down', 1 },
	},
	{
		key = 'j',
		mods = SUPER_SHIFT .. '|ALT',
		action = act.AdjustPaneSize { 'Down', 1 },
	},
	{ key = 'm', mods = SUPER_SHIFT, action = act.Hide },
	{ key = 'n', mods = SUPER_SHIFT, action = act.SpawnWindow },
	{ key = 'q', mods = SUPER_SHIFT, action = act.QuitApplication },
}

for i = 1, 9 do
	table.insert(config.keys, {
		key = tostring(i),
		mods = SUPER,
		action = act.ActivateTab(i - 1),
	})
end

config.unix_domains = {
	{
		name = 'unix',
	},
}
config.default_gui_startup_args = { 'connect', 'unix' }

return config
