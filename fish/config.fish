if status is-interactive
    # Commands to run in interactive sessions can go here
end

bind \eg lazygit
bind \ex "hx ."

alias cat="bat -pP"
alias ls="eza -F --icons -1 --total-size"
alias ll="eza -F --icons -l --total-size"
alias la="eza -F --icons -la --total-size"
alias l="eza -F --icons -laT --total-size"
alias rsync="rsync --progress"

fish_ssh_agent

zoxide init fish | source
direnv hook fish | source
